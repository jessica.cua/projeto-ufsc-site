$(function(){

    $("#tipo-cadastro-pj").on("click", function(){
        $("#data-nasc").attr("disabled", true); // desabilita o campo data de nascimento
        $("[for='cpf-cnpj']").html("<b>CNPJ:</b>"); // altera o label cpf para cnpj
        $("#data-nasc").val(""); // limpa o valor de data de nascimento
        $("#data-nasc").parent().hide(); // oculta o campo de data de nascimento
        $("#info-endereco").show();
    });

    $("#tipo-cadastro-pf").on("click", function(){
        $("#data-nasc").attr("disabled", false); // habilita o campo data de nascimento
        $("[for='cpf-cnpj']").html("<b>CPF:</b>"); // altera o label cnpj para cpf
        $("#data-nasc").parent().show(); // exibir o campo de data de nascimento
        $("#info-endereco").hide();
    });

    $("#btn-enviar").on("click", function(evento){

        if($("#senha-1").val() !== $("#senha-2").val()) {
            evento.preventDefault();
            alert("As senhas não coincidem");
        }

        var cpfCnpj = $("#cpf-cnpj").val();
        if($("#tipo-cadastro-pf").is(":checked")) {
            if(cpfCnpj.length > 0 && cpfCnpj.length !== 11) {
                evento.preventDefault();
                alert("O CPF digitado é inválido!");
            }
        } else if (cpfCnpj.length > 0 && cpfCnpj.length !== 14) {
            ($("#tipo-cadastro-pj").is(":checked"))
            evento.preventDefault();
            alert("O CNPJ digitado é inválido!");// verificar se o comprimento de cpfCnpj é 14
        }

    })

})